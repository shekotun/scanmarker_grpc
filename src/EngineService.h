//
//  OcrEngineServiceImpl.h
//  grpc-server
//
//  Created by Yury Markman on 30/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#ifndef OcrEngineServiceImpl_h
#define OcrEngineServiceImpl_h

#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>
#include "scanmarker.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;


namespace scanmarker
{

namespace Engine
{

//Service used in Engine code - must be called from UI Client

class Service final : public OcrEngineService::Service
{
public:
    Service();
    
    virtual Status Ping(ServerContext* context, const EmptyMessage* request, CommandResponse* response);
    virtual Status ConfigureEngine(ServerContext* context, const EngineConfiguration* request, CommandResponse* response);
    virtual Status ConfigureOcr(ServerContext* context, const OcrConfiguration* request,  CommandResponse* response);
    virtual Status GetScannerStatus(ServerContext* context, const EmptyMessage* request, ScannerStatus* response);
    
    bool StartService();
    bool StopService();
private:
    std::unique_ptr<ServerBuilder> m_pBuilder;
    std::unique_ptr<Server> m_server;
};
}

}
#endif /* OcrEngineServiceImpl_h */
