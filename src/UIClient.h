//
//  OcrEngineServiceClient.h
//  grpc-client
//
//  Created by Yury Markman on 31/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#ifndef OcrEngineServiceClient_h
#define OcrEngineServiceClient_h

#include <stdio.h>

#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>

#include "scanmarker.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

namespace scanmarker
{

/*
 message CommandResponse {
 ErrorCodeType error = 1;
 string message = 2;
 }
 
 fields of the message are accesssed with
 internal_error();
 and
 internal_message();
 
 methods of the class CommandResponse
 */
namespace UI
{
class Client
{
// UI Client - used from UI code. Calls Engine Service
    
public:
    Client(std::shared_ptr<Channel> channel);
    
    Status Ping(CommandResponse* response);
    
    Status ConfigureEngine(const EngineConfiguration* request, CommandResponse* response);
    Status ConfigureOcr(const OcrConfiguration* request, CommandResponse* response);
    Status GetScannerStatus(ScannerStatus* response);
        
private:
    std::unique_ptr<OcrEngineService::Stub> m_serviceStub;
};
}
}

#endif /* OcrEngineServiceClient_h */
