//
//  OcrEngineOutput.cpp
//  grpc-client
//
//  Created by Yury Markman on 31/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#include "EngineClient.h"
namespace scanmarker
{
namespace Engine
{

Client::Client(std::shared_ptr<Channel> channel)
: m_serviceStub(ocrengineclientservice::NewStub(channel))
{
    
}

Status Client::Ping(CommandResponse* response)
{
    ClientContext context;
    EmptyMessage em;
    return m_serviceStub->Ping(&context, em, response);
}

Status Client::OnscannerStatusChanged(const ScannerStatus* request)
{
    ClientContext context;
    EmptyMessage em;
    return m_serviceStub->OnscannerStatusChanged(&context, *request, &em);
}

Status Client::OnScan(const ScanData* request)
{
    ClientContext context;
    EmptyMessage em;
    return m_serviceStub->OnScan(&context, *request, &em);
}

}
}
