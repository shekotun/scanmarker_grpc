//
//  OcrEngineServiceClient.cpp
//  grpc-client
//
//  Created by Yury Markman on 31/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#include "UIClient.h"

namespace scanmarker
{
namespace UI
{
Client::Client(std::shared_ptr<Channel> channel)
: m_serviceStub(OcrEngineService::NewStub(channel))
{
    
}

Status Client::Ping(CommandResponse* response)
{
    ClientContext context;
    EmptyMessage em;
    return m_serviceStub->Ping(&context, em, response);
}

Status Client::ConfigureEngine(const EngineConfiguration* request, CommandResponse* response)
{
    ClientContext context;
    return m_serviceStub->ConfigureEngine(&context, *request, response);
}

Status Client::ConfigureOcr(const OcrConfiguration* request, CommandResponse* response)
{
    ClientContext context;
    return m_serviceStub->ConfigureOcr(&context, *request, response);
}

Status Client::GetScannerStatus(ScannerStatus* response)
{
    ClientContext context;
    EmptyMessage em;
    return m_serviceStub->GetScannerStatus(&context, em, response);
}

}

}
