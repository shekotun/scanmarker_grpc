//
//  OcrEngineOutput.hpp
//  grpc-client
//
//  Created by Yury Markman on 31/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#ifndef OcrEngineOutput_h
#define OcrEngineOutput_h

#include <stdio.h>
#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>

#include "scanmarker.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

namespace scanmarker
{
namespace Engine
{
// Engine Client - used from Engine code. Calls UI Service

class Client
{
public:
    Client(std::shared_ptr<Channel> channel);
    Status Ping(CommandResponse* response);
    Status OnscannerStatusChanged(const ScannerStatus* request);
    Status OnScan(const ScanData* request);
    
private:
    std::unique_ptr<ocrengineclientservice::Stub> m_serviceStub;
};

}
}
#endif /* OcrEngineOutput_h */
