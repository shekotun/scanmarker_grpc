//
//  main.cpp
//  grpc-client
//
//  Created by Yury Markman on 30/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#include <iostream>

#include "UIClient.h"
#include "EngineClient.h"
#include "scanmarker_grpc.h"

using namespace scanmarker;

int main(int argc, const char * argv[])
{
    CommandResponse response;
    
    ScannerStatus scannerStatusRequest;
    scannerStatusRequest.set_type(SCANNER_TYPE_AIR);
    scannerStatusRequest.set_name("Scanmarker Test from client");
    scannerStatusRequest.set_sn("77700077");
    scannerStatusRequest.set_version("5.5.5");
    scannerStatusRequest.set_state(SCANNER_STATE_CONNECTED);
    scannerStatusRequest.set_interface(SCANNER_INTERFACE_USB);
    scannerStatusRequest.set_battery(10);
    scannerStatusRequest.set_license_status(LICENSE_STATUS_TEMPORARY);
    
    google::protobuf::Timestamp *timestamp = new google::protobuf::Timestamp();
    timestamp->set_seconds(time(NULL));
    timestamp->set_nanos(0);
    
    scannerStatusRequest.set_allocated_license_expiry(timestamp);

    Status status;
    
    if (argc > 1 && !strcmp(argv[1], "UI"))
    {
        //UI client sends messages to ENGINE service
        //
        UI::Client UIClient (grpc::CreateChannel(ENGINE_SERVICE_ADDRESS, grpc::InsecureChannelCredentials()));
        
        status = UIClient.Ping(&response);
        if (!status.ok())
        {
            std::cerr << "UIClient.Ping returned error" << std::endl;
            return -1;
        }
        EngineConfiguration engineConfigurationRequest;
        
        status = UIClient.ConfigureEngine(&engineConfigurationRequest, &response);
        if (!status.ok())
        {
            std::cerr << "UIClient.ConfigureEngine returned error" << std::endl;
            return -2;
        }
        OcrConfiguration ocrConfigurationRequest;
        
        status = UIClient.ConfigureOcr(&ocrConfigurationRequest, &response);
        if (!status.ok())
        {
            std::cerr << "UIClient.ConfigureOcr returned error" << std::endl;
            return -3;
        }

        status = UIClient.GetScannerStatus(&scannerStatusRequest);
        if (!status.ok())
        {
            std::cerr << "UIClient.GetScannerStatus returned error" << std::endl;
            return -4;
        }
    }
    ///
    //////========================================================================================
    ///
    else
    {
        //Engine client sends messages to UI service
        //
        Engine::Client EngineClient (grpc::CreateChannel(UI_SERVICE_ADDRESS, grpc::InsecureChannelCredentials()));
        
        CommandResponse response;
        
        status = EngineClient.Ping(&response);
        if (!status.ok())
        {
            std::cerr << "EngineClient.Ping returned error" << std::endl;
            return -1;
        }
        
        status = EngineClient.OnscannerStatusChanged(&scannerStatusRequest);
        if (!status.ok())
        {
            std::cerr << "EngineClient.OnscannerStatusChanged returned error" << std::endl;
            return -2;
        }
        ScanData *scanDataRequest = new ScanData();
        scanDataRequest->set_scan_id(777);
        scanDataRequest->set_flags(1);
        
        ScannedText *scannedText = new ScannedText();
        scannedText->set_language("EN_US");
        scannedText->set_text("Hello, world");
        
        scanDataRequest->set_allocated_scanned_text(scannedText);
        
        status = EngineClient.OnScan(scanDataRequest);
        if (!status.ok())
        {
            std::cerr << "EngineClient.OnScan returned error" << std::endl;
            return -3;
        }
    }
    std::cout<< "Client test succeded - service stopped. Exiting" <<std::endl;
    return 0;
}
