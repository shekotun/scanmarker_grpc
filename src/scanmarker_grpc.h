//
//  scanmarker_grpc.h
//  grpc-client
//
//  Created by Yury Markman on 02/09/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#ifndef scanmarker_grpc_h
#define scanmarker_grpc_h

#define ENGINE_SERVICE_ADDRESS "0.0.0.0:50052"
#define UI_SERVICE_ADDRESS "0.0.0.0:50051"

#endif /* scanmarker_grpc_h */
