//
//  OcrEngineClientviceImp.cpp
//  grpc-server
//
//  Created by Yury Markman on 31/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#include "UIService.h"
#include "scanmarker_grpc.h"

namespace scanmarker
{
namespace UI
    {
Status
Service::Ping(ServerContext* context, const EmptyMessage* request, CommandResponse* response)
{
    if (response != nullptr)
    {
        response->set_error(ERROR_CODE_OK);
        response->set_message("OcrEngineClientServiceImp::Ping - > OK");
        return Status::OK;
    }
    else
    {
        return
            Status(grpc::INVALID_ARGUMENT, "OcrEngineClientServiceImp::Ping: response pointer is null");
    }
}

Status
Service::OnscannerStatusChanged(ServerContext* context, const ScannerStatus* request, EmptyMessage* response)
{
    if (request != nullptr)
    {
        return Status::OK;
    }
    else
    {
        return
            Status(grpc::INVALID_ARGUMENT, "OcrEngineClientServiceImp::OnscannerStatusChanged: request pointer is null");
    }
}

Status
Service::OnScan(ServerContext* context, const ScanData* request, EmptyMessage* response)
{
    if (request != nullptr)
    {
        return Status::OK;
    }
    else
    {
        return
            Status(grpc::INVALID_ARGUMENT, "OcrEngineClientServiceImp::OnScan: request pointer is null");
    }
    return Status::OK;
}

bool
Service::StartService()
{
    std::string server_address(UI_SERVICE_ADDRESS);
    
    // Listen on the given address without any authentication mechanism.
    m_pBuilder = std::unique_ptr<ServerBuilder>(new ServerBuilder());
    m_pBuilder->AddListeningPort(server_address, grpc::InsecureServerCredentials());
    
    // Register "service" as the instance through which we'll communicate with
    // clients. In this case it corresponds to an *synchronous* service.
    m_pBuilder->RegisterService(this);
    
    // Finally assemble the server.
    m_server = std::unique_ptr<Server> (m_pBuilder->BuildAndStart());
    std::cout << "OcrEngineServiceImpl listening on " << server_address << std::endl;
    
    // Wait for the server to shutdown. Note that some other thread must be
    // responsible for shutting down the server for this call to ever return.
    
    m_server->Wait();
    
    return true;
}

bool
Service::StopService()
{
    m_server->Shutdown();
    
    return true;
}
}
}
