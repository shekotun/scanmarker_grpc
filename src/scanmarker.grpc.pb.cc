// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: scanmarker.proto

#include "scanmarker.pb.h"
#include "scanmarker.grpc.pb.h"

#include <functional>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/channel_interface.h>
#include <grpcpp/impl/codegen/client_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/method_handler_impl.h>
#include <grpcpp/impl/codegen/rpc_service_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/sync_stream.h>
namespace scanmarker {

static const char* OcrEngineService_method_names[] = {
  "/scanmarker.OcrEngineService/Ping",
  "/scanmarker.OcrEngineService/ConfigureEngine",
  "/scanmarker.OcrEngineService/ConfigureOcr",
  "/scanmarker.OcrEngineService/GetScannerStatus",
};

std::unique_ptr< OcrEngineService::Stub> OcrEngineService::NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options) {
  (void)options;
  std::unique_ptr< OcrEngineService::Stub> stub(new OcrEngineService::Stub(channel));
  return stub;
}

OcrEngineService::Stub::Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel)
  : channel_(channel), rpcmethod_Ping_(OcrEngineService_method_names[0], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  , rpcmethod_ConfigureEngine_(OcrEngineService_method_names[1], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  , rpcmethod_ConfigureOcr_(OcrEngineService_method_names[2], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  , rpcmethod_GetScannerStatus_(OcrEngineService_method_names[3], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  {}

::grpc::Status OcrEngineService::Stub::Ping(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::scanmarker::CommandResponse* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_Ping_, context, request, response);
}

void OcrEngineService::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, reactor);
}

void OcrEngineService::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* OcrEngineService::Stub::AsyncPingRaw(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_Ping_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* OcrEngineService::Stub::PrepareAsyncPingRaw(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_Ping_, context, request, false);
}

::grpc::Status OcrEngineService::Stub::ConfigureEngine(::grpc::ClientContext* context, const ::scanmarker::EngineConfiguration& request, ::scanmarker::CommandResponse* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_ConfigureEngine_, context, request, response);
}

void OcrEngineService::Stub::experimental_async::ConfigureEngine(::grpc::ClientContext* context, const ::scanmarker::EngineConfiguration* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_ConfigureEngine_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::ConfigureEngine(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_ConfigureEngine_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::ConfigureEngine(::grpc::ClientContext* context, const ::scanmarker::EngineConfiguration* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_ConfigureEngine_, context, request, response, reactor);
}

void OcrEngineService::Stub::experimental_async::ConfigureEngine(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_ConfigureEngine_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* OcrEngineService::Stub::AsyncConfigureEngineRaw(::grpc::ClientContext* context, const ::scanmarker::EngineConfiguration& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_ConfigureEngine_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* OcrEngineService::Stub::PrepareAsyncConfigureEngineRaw(::grpc::ClientContext* context, const ::scanmarker::EngineConfiguration& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_ConfigureEngine_, context, request, false);
}

::grpc::Status OcrEngineService::Stub::ConfigureOcr(::grpc::ClientContext* context, const ::scanmarker::OcrConfiguration& request, ::scanmarker::CommandResponse* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_ConfigureOcr_, context, request, response);
}

void OcrEngineService::Stub::experimental_async::ConfigureOcr(::grpc::ClientContext* context, const ::scanmarker::OcrConfiguration* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_ConfigureOcr_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::ConfigureOcr(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_ConfigureOcr_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::ConfigureOcr(::grpc::ClientContext* context, const ::scanmarker::OcrConfiguration* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_ConfigureOcr_, context, request, response, reactor);
}

void OcrEngineService::Stub::experimental_async::ConfigureOcr(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_ConfigureOcr_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* OcrEngineService::Stub::AsyncConfigureOcrRaw(::grpc::ClientContext* context, const ::scanmarker::OcrConfiguration& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_ConfigureOcr_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* OcrEngineService::Stub::PrepareAsyncConfigureOcrRaw(::grpc::ClientContext* context, const ::scanmarker::OcrConfiguration& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_ConfigureOcr_, context, request, false);
}

::grpc::Status OcrEngineService::Stub::GetScannerStatus(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::scanmarker::ScannerStatus* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_GetScannerStatus_, context, request, response);
}

void OcrEngineService::Stub::experimental_async::GetScannerStatus(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::ScannerStatus* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_GetScannerStatus_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::GetScannerStatus(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::ScannerStatus* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_GetScannerStatus_, context, request, response, std::move(f));
}

void OcrEngineService::Stub::experimental_async::GetScannerStatus(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::ScannerStatus* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_GetScannerStatus_, context, request, response, reactor);
}

void OcrEngineService::Stub::experimental_async::GetScannerStatus(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::ScannerStatus* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_GetScannerStatus_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::ScannerStatus>* OcrEngineService::Stub::AsyncGetScannerStatusRaw(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::ScannerStatus>::Create(channel_.get(), cq, rpcmethod_GetScannerStatus_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::ScannerStatus>* OcrEngineService::Stub::PrepareAsyncGetScannerStatusRaw(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::ScannerStatus>::Create(channel_.get(), cq, rpcmethod_GetScannerStatus_, context, request, false);
}

OcrEngineService::Service::Service() {
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      OcrEngineService_method_names[0],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< OcrEngineService::Service, ::scanmarker::EmptyMessage, ::scanmarker::CommandResponse>(
          std::mem_fn(&OcrEngineService::Service::Ping), this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      OcrEngineService_method_names[1],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< OcrEngineService::Service, ::scanmarker::EngineConfiguration, ::scanmarker::CommandResponse>(
          std::mem_fn(&OcrEngineService::Service::ConfigureEngine), this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      OcrEngineService_method_names[2],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< OcrEngineService::Service, ::scanmarker::OcrConfiguration, ::scanmarker::CommandResponse>(
          std::mem_fn(&OcrEngineService::Service::ConfigureOcr), this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      OcrEngineService_method_names[3],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< OcrEngineService::Service, ::scanmarker::EmptyMessage, ::scanmarker::ScannerStatus>(
          std::mem_fn(&OcrEngineService::Service::GetScannerStatus), this)));
}

OcrEngineService::Service::~Service() {
}

::grpc::Status OcrEngineService::Service::Ping(::grpc::ServerContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::CommandResponse* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status OcrEngineService::Service::ConfigureEngine(::grpc::ServerContext* context, const ::scanmarker::EngineConfiguration* request, ::scanmarker::CommandResponse* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status OcrEngineService::Service::ConfigureOcr(::grpc::ServerContext* context, const ::scanmarker::OcrConfiguration* request, ::scanmarker::CommandResponse* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status OcrEngineService::Service::GetScannerStatus(::grpc::ServerContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::ScannerStatus* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}


static const char* ocrengineclientservice_method_names[] = {
  "/scanmarker.ocrengineclientservice/Ping",
  "/scanmarker.ocrengineclientservice/OnscannerStatusChanged",
  "/scanmarker.ocrengineclientservice/OnScan",
};

std::unique_ptr< ocrengineclientservice::Stub> ocrengineclientservice::NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options) {
  (void)options;
  std::unique_ptr< ocrengineclientservice::Stub> stub(new ocrengineclientservice::Stub(channel));
  return stub;
}

ocrengineclientservice::Stub::Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel)
  : channel_(channel), rpcmethod_Ping_(ocrengineclientservice_method_names[0], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  , rpcmethod_OnscannerStatusChanged_(ocrengineclientservice_method_names[1], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  , rpcmethod_OnScan_(ocrengineclientservice_method_names[2], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  {}

::grpc::Status ocrengineclientservice::Stub::Ping(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::scanmarker::CommandResponse* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_Ping_, context, request, response);
}

void ocrengineclientservice::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, std::move(f));
}

void ocrengineclientservice::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, std::move(f));
}

void ocrengineclientservice::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, reactor);
}

void ocrengineclientservice::Stub::experimental_async::Ping(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::CommandResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_Ping_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* ocrengineclientservice::Stub::AsyncPingRaw(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_Ping_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::CommandResponse>* ocrengineclientservice::Stub::PrepareAsyncPingRaw(::grpc::ClientContext* context, const ::scanmarker::EmptyMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::CommandResponse>::Create(channel_.get(), cq, rpcmethod_Ping_, context, request, false);
}

::grpc::Status ocrengineclientservice::Stub::OnscannerStatusChanged(::grpc::ClientContext* context, const ::scanmarker::ScannerStatus& request, ::scanmarker::EmptyMessage* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_OnscannerStatusChanged_, context, request, response);
}

void ocrengineclientservice::Stub::experimental_async::OnscannerStatusChanged(::grpc::ClientContext* context, const ::scanmarker::ScannerStatus* request, ::scanmarker::EmptyMessage* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_OnscannerStatusChanged_, context, request, response, std::move(f));
}

void ocrengineclientservice::Stub::experimental_async::OnscannerStatusChanged(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::EmptyMessage* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_OnscannerStatusChanged_, context, request, response, std::move(f));
}

void ocrengineclientservice::Stub::experimental_async::OnscannerStatusChanged(::grpc::ClientContext* context, const ::scanmarker::ScannerStatus* request, ::scanmarker::EmptyMessage* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_OnscannerStatusChanged_, context, request, response, reactor);
}

void ocrengineclientservice::Stub::experimental_async::OnscannerStatusChanged(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::EmptyMessage* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_OnscannerStatusChanged_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::EmptyMessage>* ocrengineclientservice::Stub::AsyncOnscannerStatusChangedRaw(::grpc::ClientContext* context, const ::scanmarker::ScannerStatus& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::EmptyMessage>::Create(channel_.get(), cq, rpcmethod_OnscannerStatusChanged_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::EmptyMessage>* ocrengineclientservice::Stub::PrepareAsyncOnscannerStatusChangedRaw(::grpc::ClientContext* context, const ::scanmarker::ScannerStatus& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::EmptyMessage>::Create(channel_.get(), cq, rpcmethod_OnscannerStatusChanged_, context, request, false);
}

::grpc::Status ocrengineclientservice::Stub::OnScan(::grpc::ClientContext* context, const ::scanmarker::ScanData& request, ::scanmarker::EmptyMessage* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_OnScan_, context, request, response);
}

void ocrengineclientservice::Stub::experimental_async::OnScan(::grpc::ClientContext* context, const ::scanmarker::ScanData* request, ::scanmarker::EmptyMessage* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_OnScan_, context, request, response, std::move(f));
}

void ocrengineclientservice::Stub::experimental_async::OnScan(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::EmptyMessage* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_OnScan_, context, request, response, std::move(f));
}

void ocrengineclientservice::Stub::experimental_async::OnScan(::grpc::ClientContext* context, const ::scanmarker::ScanData* request, ::scanmarker::EmptyMessage* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_OnScan_, context, request, response, reactor);
}

void ocrengineclientservice::Stub::experimental_async::OnScan(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::scanmarker::EmptyMessage* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_OnScan_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::EmptyMessage>* ocrengineclientservice::Stub::AsyncOnScanRaw(::grpc::ClientContext* context, const ::scanmarker::ScanData& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::EmptyMessage>::Create(channel_.get(), cq, rpcmethod_OnScan_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::scanmarker::EmptyMessage>* ocrengineclientservice::Stub::PrepareAsyncOnScanRaw(::grpc::ClientContext* context, const ::scanmarker::ScanData& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::scanmarker::EmptyMessage>::Create(channel_.get(), cq, rpcmethod_OnScan_, context, request, false);
}

ocrengineclientservice::Service::Service() {
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      ocrengineclientservice_method_names[0],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< ocrengineclientservice::Service, ::scanmarker::EmptyMessage, ::scanmarker::CommandResponse>(
          std::mem_fn(&ocrengineclientservice::Service::Ping), this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      ocrengineclientservice_method_names[1],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< ocrengineclientservice::Service, ::scanmarker::ScannerStatus, ::scanmarker::EmptyMessage>(
          std::mem_fn(&ocrengineclientservice::Service::OnscannerStatusChanged), this)));
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      ocrengineclientservice_method_names[2],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< ocrengineclientservice::Service, ::scanmarker::ScanData, ::scanmarker::EmptyMessage>(
          std::mem_fn(&ocrengineclientservice::Service::OnScan), this)));
}

ocrengineclientservice::Service::~Service() {
}

::grpc::Status ocrengineclientservice::Service::Ping(::grpc::ServerContext* context, const ::scanmarker::EmptyMessage* request, ::scanmarker::CommandResponse* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status ocrengineclientservice::Service::OnscannerStatusChanged(::grpc::ServerContext* context, const ::scanmarker::ScannerStatus* request, ::scanmarker::EmptyMessage* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}

::grpc::Status ocrengineclientservice::Service::OnScan(::grpc::ServerContext* context, const ::scanmarker::ScanData* request, ::scanmarker::EmptyMessage* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}


}  // namespace scanmarker

