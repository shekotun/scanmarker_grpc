//
//  main.cpp
//  grpc-server
//
//  Created by Yury Markman on 30/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#include <iostream>
#include "UIService.h"

#include "EngineService.h"

using namespace scanmarker;

int main(int argc, const char * argv[]) {
    if (argc > 1 && !strcmp(argv[1], "UI"))
    {
        UI::Service uiService;
        uiService.StartService();
        getchar();
        uiService.StopService();
    }
    else
    {
        Engine::Service engineService;
        engineService.StartService();
        getchar();
        engineService.StopService();
    }
    std::cout<< "Server test - service stopped. Exiting" <<std::endl;
    return 0;
}
