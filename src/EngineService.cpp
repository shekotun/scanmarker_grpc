//
//  OcrEngineServiceImpl.cpp
//  grpc-server
//
//  Created by Yury Markman on 30/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#include <stdio.h>
#include "EngineService.h"

#include "scanmarker_grpc.h"

namespace scanmarker
{
namespace Engine
{
Service::Service()
{
    
}

Status
Service::Ping(ServerContext* context, const EmptyMessage* request, CommandResponse* response)
{
    if (response != nullptr)
    {
        response->set_error(ERROR_CODE_OK);
        response->set_message("OcrEngineServiceImpl::Ping - > OK");
        return Status::OK;
    }
    else
    {
        return
            Status(grpc::INVALID_ARGUMENT, "OcrEngineServiceImpl::Ping: response pointer is null");
    }
}

Status
Service::ConfigureEngine(ServerContext* context, const EngineConfiguration* request, CommandResponse* response)
{
    if (response != nullptr && request != nullptr)
    {
        response->set_error(ERROR_CODE_OK);
        response->set_message("OcrEngineServiceImpl::ConfigureEngine - > OK");
        return Status::OK;
    }
    else
    {
        return
        Status(grpc::INVALID_ARGUMENT, "OcrEngineServiceImpl::ConfigureEngine: response pointer is null");
    }
}

Status
Service::ConfigureOcr(ServerContext* context, const OcrConfiguration* request,  CommandResponse* response)
{
    if (response != nullptr && request != nullptr)
    {
        response->set_error(ERROR_CODE_OK);
        response->set_message("OcrEngineServiceImpl::ConfigureOcr - > OK");
        return Status::OK;
    }
    else
    {
        return
        Status(grpc::INVALID_ARGUMENT, "OcrEngineServiceImpl::ConfigureOcr: response pointer is null");
    }
}

Status
Service::GetScannerStatus(ServerContext* context, const EmptyMessage* request, ScannerStatus* response)
{
    if (response != nullptr)
    {
        response->set_type(SCANNER_TYPE_AIR);
        response->set_name("Scanmaerker Air test API");
        response->set_sn("000077777");
        response->set_version("777");
        response->set_state(SCANNER_STATE_CONNECTED);
        response->set_interface(SCANNER_INTERFACE_BLE);
        response->set_battery(50);
        response->set_license_status(LICENSE_STATUS_VALID);
        
        google::protobuf::Timestamp *timestamp = new google::protobuf::Timestamp();
        timestamp->set_seconds(time(NULL));
        timestamp->set_nanos(0);
        
        response->set_allocated_license_expiry(timestamp);
        
        return Status::OK;
    }
    else
    {
        return
            Status(grpc::INVALID_ARGUMENT, "OcrEngineServiceImpl::ConfigureOcr: response pointer is null");
    }
}

bool
Service::StartService()
{
    std::string server_address(ENGINE_SERVICE_ADDRESS);

    // Listen on the given address without any authentication mechanism.
    m_pBuilder = std::unique_ptr<ServerBuilder>(new ServerBuilder());
    m_pBuilder->AddListeningPort(server_address, grpc::InsecureServerCredentials());
    
    // Register "service" as the instance through which we'll communicate with
    // clients. In this case it corresponds to an *synchronous* service.
    m_pBuilder->RegisterService(this);
    
    // Finally assemble the server.
    m_server = std::unique_ptr<Server> (m_pBuilder->BuildAndStart());
    std::cout << "OcrEngineServiceImpl listening on " << server_address << std::endl;
    
    // Wait for the server to shutdown. Note that some other thread must be
    // responsible for shutting down the server for this call to ever return.
    
    m_server->Wait();
    
    return true;
}

bool
Service::StopService()
{
    m_server->Shutdown();
    
    return true;
}
}
}
