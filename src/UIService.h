//
//  OcrEngineClientServiceImp.hpp
//  grpc-server
//
//  Created by Yury Markman on 31/08/2019.
//  Copyright © 2019 Scanmarker. All rights reserved.
//

#ifndef OcrEngineClientServiceImp_hpp
#define OcrEngineClientServiceImp_hpp

#include <stdio.h>

#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>
#include "scanmarker.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;



namespace scanmarker
{

//Service used in UI code - must be called from Engine Client

namespace UI
{
class Service final : public ocrengineclientservice::Service
{
public:
    virtual Status Ping(ServerContext* context, const EmptyMessage* request, CommandResponse* response);
    virtual Status OnscannerStatusChanged(ServerContext* context, const ScannerStatus* request, EmptyMessage* response);
    virtual Status OnScan(ServerContext* context, const ScanData* request, EmptyMessage* response);
    
    bool StartService();
    bool StopService();

private:
    std::unique_ptr<ServerBuilder> m_pBuilder;
    std::unique_ptr<Server> m_server;
};
}
}

#endif /* OcrEngineClientServiceImp_hpp */
